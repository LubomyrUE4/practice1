package com.java.basic.practice1;

public class Part7 {
    public static void main(String[] args) {
        System.out.println("Str to Int: " + str2int(args[0]));
        System.out.println("Int to Str: " + int2str(Integer.parseInt(args[1])));
        System.out.println("rightColumn: " + rightColumn(args[2]));
    }
    public static int str2int(String number) {
        char[] ch = new char[number.length()];
        for (int i = 0; i < number.length(); i++) {
            ch[i] = number.charAt(i);
        }
        return (ch.length - 1) * 26 + ch[ch.length - 1] - 64;
    }

    public static String int2str(int number) {
        char[] ch = new char[100];
        String result = "";
        if (number <= 26 && number > 0) {
            char c = (char) (64 + number);
            result += Character.toString(c);
        } else {
            for (int i = 0; i < number/26; i++) {

                result += Character.toString((char) (65 + i));
            }
            result += Character.toString((char) 64 + number%26);
        }
        return result;
    }

    public static String rightColumn(String number) {
        return int2str(str2int(number) + 1);
    }
}
