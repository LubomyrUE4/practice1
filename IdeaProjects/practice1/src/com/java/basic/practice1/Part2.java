package com.java.basic.practice1;
import java.util.Scanner;

class Part2 {
	public static void main(String[] args) {
		int a, b;
		a = Integer.parseInt(args[0]);
		b = Integer.parseInt(args[1]);
		System.out.print("Sum = " + (a + b));
	}
}
