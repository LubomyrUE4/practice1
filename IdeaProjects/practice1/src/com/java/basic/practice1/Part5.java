package com.java.basic.practice1;

import java.util.Scanner;

public class Part5 {

    public static void main(String[] args) {
        String a = args[0];
        int s = 0;
        String b;
        try{
            for (int i = 0; i < a.length(); i++) {
                b = "" + a.charAt(i);
                s += Integer.parseInt(b);
            }

            System.out.println(s);
        }
        catch (Exception ex){
            System.out.println("It is not a number");
        }
    }
}
