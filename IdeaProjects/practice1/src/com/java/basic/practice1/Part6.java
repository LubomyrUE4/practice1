package com.java.basic.practice1;

import java.util.Scanner;

public class Part6 {

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        int[] a = new int[n];
        int s = 0;
        int m = 0;
        for (int i = 2; i < n; i++) {
            s = 0;
            for (int j = 2; j <= i; j++) {
                if (i%j == 0) {
                    s++;
                }
            }
            if(s < 2) {
                a[m] = i;
                m++;
            }
        }
        for (int i = 0; i < m; i++) {
            if (i != m - 1) {
                System.out.print(a[i] + ", ");
            } else {
                System.out.print(a[i]);
            }

        }


    }
}
